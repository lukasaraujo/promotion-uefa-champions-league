var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    cleanCSS = require('gulp-clean-css'),
    plumber = require('gulp-plumber'),
    coffee = require('gulp-coffee'),
    imagemin = require('gulp-imagemin');

gulp.task('teste', function () {
    console.log('Gulp Funcionando...');
})

//Compilando SASS
gulp.task('sass', function () {
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass())

        .pipe(gulp.dest('./src/css/'));

})
//Compilando SASS

//Monitorando Somente SASS
gulp.task('listen', function(){
    gulp.watch('./src/sass/**/*.scss', ['sass'])
})
//Monitorando Somente SASS


//Iniciar BrowserSync
gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: 'src'
        }
    })
    gulp.watch('./src/css/*.css').on('change', browserSync.reload)
    gulp.watch('./src/index.html').on('change', browserSync.reload)
    
    gulp.watch('./src/sass/**/*.scss', ['sass'])
})
//Iniciar BrowserSync

//Copiar Arquivos para pasta de desenvolvimento
gulp.task('copy', ['minify-css','images','clean'], function () {
    gulp.src(['src/css/**/*', 'src/*.html','src/js/**/*'], {
            "base": "src"
        })
        .pipe(gulp.dest('dist'))
})
//Copiar Arquivos para pasta de desenvolvimento

//Limpar arquivos antes de copiar
gulp.task('clean', function () {
    return gulp.src('dist')
        .pipe(clean());

})
//Limpar arquivos antes de copiar

//Minificar Arquivos CSS JS html
gulp.task('minify-css', ['default'], function () {
    return gulp.src('src/css/*.css')
        .pipe(plumber())
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest('src/css'));
});
//Minificar Arquivos CSS JS html

//Adicionar webkits arquivos css
gulp.task('default', function () {
    return gulp.src('src/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('src/css/'))
});

//Adicionar webkits arquivos css


gulp.task('images', () =>
    gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
);
